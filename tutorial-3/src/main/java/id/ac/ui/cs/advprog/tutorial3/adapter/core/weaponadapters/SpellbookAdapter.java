package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {
    private Spellbook spellbook;
    private Boolean normie;
    

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.normie = true;
    }

    @Override
    public String normalAttack() {
        normie = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (normie) {
            normie = false;
            return spellbook.largeSpell();
        }
        return "Magic power is not enough for large spell";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
