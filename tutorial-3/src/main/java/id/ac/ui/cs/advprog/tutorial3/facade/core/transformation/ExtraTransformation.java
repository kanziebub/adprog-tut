package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini mengimplementasikan sistem kriptografi Caesar Cipher (shift 11)
*/

public class ExtraTransformation {
    private int shift;

    public ExtraTransformation() {
        this.shift = 11;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, Boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;

        int codexSize = codex.getCharSize();
        char[] res = new char[text.length()];

        for (int i = 0; i < text.length(); i++) {
            char prevChar = text.charAt(i);
            int prevIdx = codex.getIndex(prevChar);
            int newIdx = selector * shift + prevIdx;
            newIdx = newIdx < 0 ? codexSize + newIdx : newIdx % codexSize;
            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);
    }

}
