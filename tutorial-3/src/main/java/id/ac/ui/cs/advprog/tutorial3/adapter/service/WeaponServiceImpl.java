package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;

import java.util.List;

// Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    private boolean init = false;

    @Override
    public List<Weapon> findAll() {
        if(!init) {
            List<Bow> listBow = bowRepository.findAll();
            for(Bow bow : listBow) {
                BowAdapter bowAdapter = new BowAdapter(bow);
                weaponRepository.save(bowAdapter);
            }
    
            List<Spellbook> listSpellbook = spellbookRepository.findAll();
            for(Spellbook spellbook : listSpellbook) {
                SpellbookAdapter spellbookAdapter = new SpellbookAdapter(spellbook);
                weaponRepository.save(spellbookAdapter);
            }

            init = true;
        }

        // fetch weapon repo
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        String hnm = weapon.getHolderName();
        String wnm = weapon.getName(); 
        String atk;
        if (attackType == 1) {
            atk = weapon.chargedAttack();
            logRepository.addLog(hnm + " attacked with " + wnm + " (charged attack): " + atk);
        } else if (attackType == 2) {
            atk = weapon.normalAttack();
            logRepository.addLog(hnm + " attacked with " + wnm + " (normal attack): " + atk);
        }
        weaponRepository.save(weapon);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
